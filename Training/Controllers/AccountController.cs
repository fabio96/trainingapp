﻿using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Security;
using Training.App_Start;
using Microsoft.AspNet.Identity.Owin;
using Training.Models;
using Training.models;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;

namespace Training.Controllers
{
    [Authorize]
    [RoutePrefix("api/v1/account")]
    public class AccountController : ApiController
    {
        private const string LocalLoginProvider = "Local";
        private ApplicationUserManager _userManager;

        public AccountController() { }

        public AccountController(ApplicationUserManager userManager, ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }
        public object GetErrorResult { get; private set; }

        [AllowAnonymous]
        [Route("")]
        public async Task<IHttpActionResult> Register(RegisterBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new ApplicationUser { UserName = model.email, Email = model.email };
            IdentityResult result = await UserManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
            {
                return InternalServerError();
            }
            return Ok();
        }
    }
}
