﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Training.models.IdentityContext
{
    public class ApplicationDBContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDBContext() : base("DefaultConnection", throwIfV1Schema: false) { }

        public static ApplicationDBContext Create()
        {
            return new ApplicationDBContext();
        }
    }
}
